import { StatusBar } from 'expo-status-bar';
import React, { useState, useEffect } from 'react';
import { ImageBackground, StyleSheet, Text, View, Image, ScrollView} from 'react-native';
import Icons from './components/Icons';
import Modalmessage from './components/Modalmessage';
import Modaldesc from './components/Modaldesc';
import Modalsearch from './components/Modalsearch';
import * as Location from 'expo-location';


const image ={ uri: "https://res.cloudinary.com/marcelakor/image/upload/v1622470849/samples/weather-app/screen.jpg"}
const API_KEY = '5cff3a5f370fd2464596052cXXX';

export default function App(props) {
	const [ location, setLocation ] = useState(null);
	const [ errorMsg, setErrorMsg ] = useState(null);
	const [ weatherinfo, setWeatherinfo ] = useState({
		city: '',
		country: '',
		icon: '',
		temp: '',
		desc: '',
		humidity: '',
		main: '',	
	});
	const [ weatherdays, setWeatherdays ] = useState([]);
	
	useEffect(() => {
		(async () => {
			let { status } = await Location.requestForegroundPermissionsAsync();
			if (status !== 'granted') {
				setErrorMsg('Permission to access location was denied');
				return;
			}
			let location = await Location.getCurrentPositionAsync({});
			getWeatherData(location.coords.latitude, location.coords.longitude);
			console.log('location ====>', location);
			setLocation(location);
		})();
	}, []);
	const getWeatherData = async (latitude, longitude) => {
		try {
			const owmUrl = `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=${API_KEY}&units=metric`;
			const r = await fetch(owmUrl);
			const res = await r.json();
     		const owmUrl2 = `https://api.openweathermap.org/data/2.5/forecast?lat=${latitude}&lon=${longitude}&appid=${API_KEY}&units=metri&forecastDays=5`;
			const r2 = await fetch(owmUrl2)
			const res2 = await r2.json()
        console.log('res2 ==>',res2)
			setWeatherinfo({
				city: res.name,
				country: res.sys.country,
				icon: res.weather[0].icon,
				temp: res.main.temp,
				desc: res.weather[0].description,
				humidity: res.main.humidity,
				main: res.weather[0].main,
			});
			setWeatherdays({
				city: res2.city.name,
				icon: res2.list[0].icon,
				temp: res2.list[0].main.temp,
				desc: res2.list[0].weather.main,
				data: res2.list[0].dt_txt,
			});
		} catch (error) {
			console.log('error ======>', error);
		}
	};
	let text = 'Waiting..';
	if (errorMsg) {
		text = errorMsg;
	} else if (location) {
		text = JSON.stringify(location);
	}

//	formatDay = () => {
//		for (list in res2) {
//			return weatherdays.slice(0,5).map((ele ) => (
//				<ScrollView>
//					<Image style={{ width: 100, height: 100 }} 
//					source={{ uri: `http://openweathermap.org/img/w/${weatherinfo.icon}.png` }} />
//						City: {ele.city.name}
//						Temp: {ele.main.temp} ºC
//						Data: {ele.dt_txt}
//				</ScrollView>
//			) )
//		}
//	}



	return (
		<View style={styles.container}>
			<ImageBackground source={image} style={styles.image}>
			
			<Modalsearch getWeatherData={getWeatherData} weatherdays={weatherdays} />

				<View style={styles.conteinertext}>
				<Image 
						style={{ width: 100, height: 100 }}
						source={{
							uri: Icons[weatherinfo.icon]
						}} />

					<Text style={styles.text}>City: {weatherinfo.city}</Text>
					<Text style={styles.text}>Temp: {weatherinfo.temp} ºC</Text> 
				</View>

				<View style={styles.modalicons}>
					<Modaldesc weatherinfo={weatherinfo} />
					<Modalmessage />
				</View>
				<StatusBar style="auto" />	
			</ImageBackground>
		</View>
	);
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
	height: '33%', 
  },
  image: {
    flex: 1,
    resizeMode: "cover",
    justifyContent: "center"
  },
  text: {
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 18,
    marginTop: 0,
    width: 200,
    color: 'white'
  },
  conteinertext: {
	height: '33%', 
    alignItems: "center",
    shadowColor: "#000",
  },
  modalicons: { 
	height: '33%', 
    flexDirection: 'row',
    justifyContent: 'space-between',
  }
});