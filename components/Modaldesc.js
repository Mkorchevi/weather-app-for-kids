import React, { useState, useEffect } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View } from "react-native";

const Modaldesc = (props) => {
  const [modalVisible, setModalVisible] = useState(false);
  const [weatherinfo, setWeatherinfo] = useState({
    city: '',
    country:'',
    temp: '',
    main: '',
    humidity: '',
    icon: '',
    desc:'',
  });

  useEffect( () => {
    setWeatherinfo(props.weatherinfo)
    console.log('weather info', weatherinfo)
    },[props.weatherinfo]);

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>City: {weatherinfo.city} {"\n"}
              Country: {weatherinfo.country} {"\n"}
              Temperature: {weatherinfo.temp} ºC {"\n"}
              Description: {weatherinfo.desc} {"\n"}
              Humidity: {weatherinfo.humidity} % </Text> 
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Close</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
      <Pressable
        style={[styles.button, styles.buttonOpen]}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.textStyle}>Weather description</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#B9DCE3",
  },
  buttonClose: {
    backgroundColor: "#F3D6D7",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "justify"
  }
});

export default Modaldesc;