import React, { useState, useEffect } from "react";
import { Alert, Modal, StyleSheet, Text, Pressable, View, ScrollView} from "react-native";
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';


const Modalsearch = (props) => {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <View style={styles.centeredView}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          Alert.alert("Modal has been closed.");
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalText}>
              {<GooglePlacesAutocomplete 
					placeholder="Search"
        	  onPress={async (data, details = null) => {
           	  const resJson = await fetch(
              `https://maps.googleapis.com/maps/api/geocode/json?place_id=${data.place_id}&key=AIzaSyBOgnH4CHMSIhlmo7dJI8hhHEeXXXXX`
            );
            const response = await resJson.json();
            console.log("response =====>", response);
            const {lat,lng} = response.results[0].geometry.location
            props.getWeatherData(lat,lng)
          }}
					query={{
						key: 'AIzaSyBOgnH4CHMSIhlmo7dJI8hhHEe6XXXXX',
						language: 'en',
					}}
					currentLocation={true}
					currentLocationLabel="Current location"
				/>}

        </Text>
            <Pressable
              style={[styles.button, styles.buttonClose]}
              onPress={() => setModalVisible(!modalVisible)}
            >
              <Text style={styles.textStyle}>Close</Text>
            </Pressable>
          </View>
        </View>
      </Modal>
      <Pressable
        style={[styles.button, styles.buttonOpen]}
        onPress={() => setModalVisible(true)}
      >
        <Text style={styles.textStyle}>Search a City...</Text>
      </Pressable>
    </View>
  );
};

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  button: {
    borderRadius: 20,
    padding: 10,
    elevation: 2
  },
  buttonOpen: {
    backgroundColor: "#B9DCE3",
  },
  buttonClose: {
    backgroundColor: "#F3D6D7",
  },
  textStyle: {
    color: "white",
    fontWeight: "bold",
    textAlign: "center"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center"
  }
});

export default Modalsearch;